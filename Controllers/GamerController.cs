using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using gamer_core.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace gamer_core.Controllers
{

    [Route("api/[controller]")]
    public class GamerController : Controller
    {

        private readonly GamerContext _context;

        public GamerController(GamerContext context)
        {
            _context = context;
        }



        // esta es la forma de obtener datos desde .NET core a mysql
        [HttpGet]
        public IEnumerable<Gamer> Get()
        {
            return _context.gamer.ToList();
        }

        [HttpGet("{id}")]           
        public ActionResult GetTodoItem(int id)
        {
            var g = _context.gamer.Find(id);

            if (g == null)
            {
                return NotFound("404 - Not Found");
            }

            return new ObjectResult(g);
        }

        [HttpPost]
        public ActionResult Post([FromBody] Gamer g)
        {
            _context.gamer.Add(g);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetTodoItem), new { id = g.Id }, g);
        }

        [HttpPut("{id}")]
        public ActionResult PutTodoItem(int id, [FromBody] Gamer g)
        {
            if (id != g.Id)
            {
                return BadRequest("400 - Bad Request");
            }
            _context.Entry(g).State = EntityState.Modified;
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteTodoItem(int id)
        {
            var todoItem = _context.gamer.Find(id);

            if (todoItem == null)
            {
                return NotFound("404 - Not Found");
            }

            _context.gamer.Remove(todoItem);
            _context.SaveChanges();

            return NoContent();
        }
    }

}
//dotnet restore
//dotnet build