using System.ComponentModel.DataAnnotations;

namespace gamer_core.Models
{
    public class Gamer
    {
        [Key]
        public int Id {get; set;}
        public string Nombre {get; set;}
        public int Mmr {get; set;}
        public string Nickname {get; set;}
        public string Rol {get; set;}
    }
}