using Microsoft.EntityFrameworkCore;


namespace gamer_core.Models
{
    public class GamerContext : DbContext
    {
        public GamerContext(DbContextOptions<GamerContext> options)
            : base(options){}

        public DbSet<Gamer> gamer {get; set;}
    }
}